nombreclase=$1
tiempo=$2
literals=$3
classpath=$4

if [ $# -lt 2 ]; then
  echo "Numero ilegal de parametros"
  exit
elif [ $# -ne 3 ];then
  java -cp epasTest.jar main.EPAGeneration $nombreclase $tiempo
elif [ $# -ne 4 ];then
  java -cp epasTest.jar main.EPAGeneration $nombreclase $tiempo --literals $literals
elif [ $# -ne 5 ];then
  java -cp epasTest.jar:$classpath/  main.EPAGeneration $nombreclase $tiempo --literals $literals --path $classpath
fi

rm -r bin 

dot -T pdf ./output/$nombreclase.dot -o ./output/$nombreclase.pdf

case "$OSTYPE" in
  darwin*)  open ./output/$nombreclase.pdf;; 
  linux*)   xdg-open ./output/$nombreclase.pdf;;
esac
