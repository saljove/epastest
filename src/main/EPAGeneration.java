package main;

import java.util.ArrayList;

import enabledMovement.EnabledMovementList;
import epa.EPA;
import utils.ConfigLoader;
import utils.TestGeneration;


/**
 * Clase Principal
 * @author Salvador Jove, Mathias Bottero
 * @version 1.0
 */
public class EPAGeneration {

	/*Almacena los métodos públicos de la clase de interés*/
	private static ArrayList<String> methods = new ArrayList<String>();
	
	/**
	 * Usados para almacenar el tiempo de inicio del proceso de construcción
	 * y el tiempo total
	 */
	private static long executionTime;
	private static long startExecTime;

	/**
	 * Método que inicia la construcción de la EPA
	 * @param className nombre de la clase (full classname) de la clase de interés.
	 * @param generationTime tiempo destinado a la generación de test (randoop timeout)
	 * @param literalFile archivo con los literales que utilizará randoop para generar sus tests.
	 * @param classpath path donde se encuentran los binarios del caso de estudio.
	 * @return la EPA obtenida a partir de los test generados automáticamente.
	 * @exception ClassNotFoundException si className no existe
	 */
	public static EPA start(String className, Integer generationTime, String literalFile, String classpath){

		ConfigLoader cm;
		try {
			cm = new ConfigLoader(className);

			methods = cm.getMethods();
			startTime();

			TestGeneration tg;
			tg = new TestGeneration(className);
			tg.generateTest(generationTime,literalFile,classpath);
		}		
		catch (ClassNotFoundException e) {
			System.out.println(className + ": Clase no encontrada");
		}
  		EnabledMovementList tls = new EnabledMovementList();
		EPABuilder builder = new EPABuilder(className, methods);
		int testCount = builder.calculateEnabledMoves(tls);

		EPA epa = builder.EPABuilder(tls);

		endTime();
				
		System.out.println("End of Epa Generator Execution");
		System.out.println("Test numbers processed by Epas: " + testCount);
		System.out.println("Overall time: " + getExecutionTime() / 1000.0 + " s.");

		return epa;
	}
	
	
	/** 
	 * @param args 0: full className de la clase sobre la que se quiere construir la EPA
	 * @param args 1:tiempo límite destinado a la generación de test
	 * @throws ClassNotFoundException 
	 */
	public static void main (String [ ] args){
		if(args.length<2) {
			throw new Error("Incorrect usage! Class name and test generation time must be passed as parameters");
		}
		String className = args[0];
		String literalFile =null;
		String classpath =null;
		Integer generationTime =null;
		try {
			generationTime = Integer.parseInt(args[1]);
			if(args.length>2) {
					String lit =args[2];
					if(lit.equals("--literals")) {
						if(args.length<4) {
							throw new Error("file name is required for --literals option");
						}else {
							literalFile =args[3];		
						}
					}
			}
			if(args.length>4) {
				String pat= args[4];
				if(pat.equals("--path")){
					if(args.length <6) 
						throw new Error("path to classes  is required for --path option");
					else 
					    classpath =args[5];
						
				}								
			}

			
			EPA result;
			result = start(className, generationTime, literalFile, classpath);
			result.generateDOT("output/"+ className+".dot");

			
		 
		} catch (NumberFormatException e) {
			System.out.println(generationTime + ": must be a name");
		}

	}

	/**
	 * Helper Methods: Usados para calcular tiempo de ejecución total
	 * */
	
	/**
	 * Método que retorna el tiempo de ejecución total
	 * @return tiempo de ejecución total
	 */
	private static long getExecutionTime() {
		return executionTime;
	}    

	/**
	 * Método para iniciar el conteo de tiempo
	 */
	private static void startTime() {
		startExecTime = System.currentTimeMillis();
	}

	/**
	 * Método para finalizar el conteo del tiempo de ejecución
	 */
	private static void endTime() {
		long endExecTime = System.currentTimeMillis();
		executionTime = endExecTime - startExecTime;
	}	

	
}
