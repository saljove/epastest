package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import enabledMovement.EnabledMovement;
import enabledMovement.EnabledMovementList;
import epa.EPA;
import epa.EPAException;
import epa.Node;
import epa.Transition;
import utils.Tuple;


/**
 * Clase responsable de leer los test generados por randoop y analizar
 * cuales son los movimientos habilitados en la EPA según esos test
 * @author Salvador Jove, Mathias Bottero
 * @version 1.0
 */
public class EPABuilder {

	/**
	 *full class Name y métodos de la clase sobre la cual se construirá la EPA 
	 **/
	private String fullClassName;
	private ArrayList<String> methods;

	
	private static Pattern patternInitialTest;
	private static Pattern patternInitialConstructor;
	private static Pattern patternMethod;	
	
	/**
	 * Constructor 
	 * @param classname el nombre (full) de la clase
	 * @param methods métodos de la clase
	 */
	public EPABuilder(String className, ArrayList<String> methods) {
		this.fullClassName = className;
		this.methods = methods;
		
		/**
		 * Se inicializan algunas expresiones regulares utilizadas 
		 * cuando se recorren los test generados automáticamente
		 */
		final String regexInitialTest = "public void\\s?([a-zA-Z0-9]+)\\(\\)";
		final String regexInitialConstructor = className+"\\s?([a-zA-Z0-9]+)\\s?=\\s?new\\s?" + className;
		patternInitialTest = Pattern.compile(regexInitialTest);
		patternInitialConstructor = Pattern.compile(regexInitialConstructor);
	}	

	/**
	 * Método que calcula el listado de movimientos habilitados en la EPA
	 * según los test generados por randoop
	 * @param tls El parametro tls representa una lista de secuencias
	 * @return el numero de secuencias
	 * @exception IOException en error de entrada.
	 * @throws ClassNotFoundException 
	 */
	public int calculateEnabledMoves(EnabledMovementList tls){
		String packName = getPackageName();
		String path = packName.replace(".", "/");
		int ts = 0; //Cantidad de junit testcases generados						
		int countSequence=0;		
		String fileName = "output/" + path+ "/RegressionTest"+ts+".java";
		String outputFileName = "output/" + path+ "/TEST-"+packName +".RegressionTest"+ts+".txt";						

		File file = new File(fileName);
		File outputFile = new File(outputFileName);
		
		//for  all testcases
		while((file.exists())&(outputFile.exists())) {	
		 try {	
				//test		
				FileReader fr = new FileReader(file);
				BufferedReader br = new BufferedReader(fr);
				String line;
	
				//test output		
				FileReader ofr = new FileReader(outputFile);
				BufferedReader obr = new BufferedReader(ofr);
	
				String oline = obr.readLine();
				while ((oline != null) && (!oline.contains("Standard Output"))) {
					oline =obr.readLine();
				}
				if ((oline =obr.readLine())!=null) {
					while ((line = br.readLine()) != null ) {
	
						//find a test
						Matcher m = patternInitialTest.matcher(line);			
						if (m.find ()) {
							tls.addAll(calculateEnabledMovesByTest(br, line,obr,oline));
							countSequence++;
						}
					}
		
					ts++;
		
					fileName = "output/" + path+ "/RegressionTest"+ts+".java";
					outputFileName = "output/" + path+ "/TEST-"+getPackageName() +".RegressionTest"+ts+".txt";									
					file = new File(fileName);
					outputFile = new File(outputFileName);
				}
		 	}catch(IOException e) {
				System.out.println("No test cases found");
			}

		}

		return countSequence;
	}
	
	/**
	 * Metodo que dado el listado de movimientos habilitados en la EPA, construye el grafo
	 * que representa la EPA
	 * @param enabledMovements lista de movimientos habilitados calculados  a partir de los test
	 * @return una EPA (grafo) que  contiene todos los movimientos habilitados como transiciones entre nodos 
	 * @exception 
	 */
	public EPA EPABuilder(EnabledMovementList enabledMovements){
		
		EPA epa = new EPA();			
		try {
			for(EnabledMovement s: enabledMovements) {
				String start = s.getStart();
				Node node_1 = new Node(start);
				if (node_1.getName().equals(fullClassName)) 
					node_1.setInitial(true);
				epa.addNode(node_1);
				String end = s.getEnd();
				Node node_2 = new Node(end);
				epa.addNode(node_2);
				Transition t =  new Transition(node_1, s.getInstruction(), node_2);			
				epa.addTransition(t);
			}
		}catch(EPAException e) {
			System.out.println(e.getMessage());
		}	
		return epa;
	}


	/**
	 * Método que calcula un listado de movimientos habilitados según un test individual
	 * @param BufferedReader para leer archivo donde se encontran los test
	 * @param currentLine línea en el archivo donde se encontra el test bajo análisis 
	 * @param obr BufferedReader para leer archivo donde se encontran la salida de los test
	 * @param currentOutputLine línea en el archivo donde se encontra la salida del test bajo análisis
	 * @return listado de movimientos habilitados calculados a partir del  test analizado
	 */
	private  EnabledMovementList calculateEnabledMovesByTest(BufferedReader br, String currentLine, BufferedReader obr, String currentOutputLine) throws IOException {

		//ListSequence por cada objeto creado
		HashMap<String, EnabledMovementList> sequencesByObjects =new HashMap<String, EnabledMovementList>();
		//estado previo por cada objeto
		HashMap<String,String> objectList= new HashMap<String,String>();

		String currentState = currentOutputLine;
		while ((currentLine != null && !currentLine.contains("@Test")) && (currentState!=null)){

			currentState = currentState.replaceAll("\\.","");

			Matcher m = patternInitialConstructor.matcher(currentLine);
			if (m.find ()) { //la línea coincide con la llamada a un constructor de la clase de interés
				EnabledMovement sequence = new EnabledMovement();
				sequence.setStart(fullClassName);
				sequence.setInstruction(fullClassName);
				sequence.setEnd(currentState);
				String objectName = m.group(1);
				if(!sequencesByObjects.containsKey(objectName)) {
					EnabledMovementList l = new EnabledMovementList();
					l.add(sequence);
					sequencesByObjects.put(objectName,l);
				}else {
					EnabledMovementList l = sequencesByObjects.get(objectName);
					l.add(sequence);
				}	
				objectList.put(objectName,currentState); //previous state for objectName
				currentState =obr.readLine();
			}else { //no es un constructor
				Tuple <String, String> t = isRelevantMethod(objectList.keySet(), currentLine);
				if(t!=null) { //object and method to be taken in account
					String objectName = t.first();
					String method = t.second();
					if(methods.contains(method)) {
						EnabledMovement sequence = new EnabledMovement();
						sequence.setStart(objectList.get(objectName));
						sequence.setInstruction(method);
						sequence.setEnd(currentState);
						if(!sequencesByObjects.containsKey(objectName)) {
							EnabledMovementList l = new EnabledMovementList();
							l.add(sequence);
							sequencesByObjects.put(objectName,l);
						}else {
							EnabledMovementList l = sequencesByObjects.get(objectName);
							l.add(sequence);
						}	
						objectList.put(objectName,currentState); //previous state for objectName
						currentState =obr.readLine();
					}
				}
			}		 
			currentLine =br.readLine();	
		}

		EnabledMovementList l = new EnabledMovementList();
		for(EnabledMovementList list: sequencesByObjects.values()) {
			l.addAll(list);
		}
		return l;
	}

	
	
	/**
	 * Helper methods 
	 */
	
	
	/**
	 * Calcula si el string dado corresponde a una invocación de un método 
	 * de la clase de interés sobre alguno de los objetos previamente creados en el test
	 * en tal caso retorna un par conteniendo el nombre del método y el nombre del
	 * objeto sobre el cual éste fue invocado.
	 * @param objects objetos creados en el test previamente
	 * @param line string conteniendo una línea en un test
	 * @return nombre del método y el nombre del objeto sobre el cual el método fue invocado
	 * o null en el caso que el string no corresponda a una invocación de un  método de la clase de interés
	 */
	private  static Tuple<String,String> isRelevantMethod(Set<String> objects, String line) {
		Matcher m =null;
		for(String obj: objects) {
			String regexMethod =  obj+"\\.([a-zA-Z0-9]+)"; 
			patternMethod = Pattern.compile(regexMethod);
			m = patternMethod.matcher(line);
			if(m.find()) {
				return new Tuple<String,String>(obj,m.group(1));
			}	
		}
		return null;
	}
	
	

  	private  String getPackageName() {
  		String pack =null;
  		try {
  			pack = Class.forName(fullClassName).getPackage().getName();
  		}catch(ClassNotFoundException e){
  			System.out.println(fullClassName + "class ot found");
  		}	
  		return pack;
  	}
}
