package miPila.utils;

public class PreconditionException extends RuntimeException {

	public PreconditionException(String s) {
		super(s);
	} // fin constructor

}
