package miPila.utils;

import randoop.CheckRep;
import java.util.Set;

/**
 * Implementacion de Pila, usando una lista simplemente encadenada.
 */
public class Pila {

	private Entry top; // usado como la cabeza del a lista enlazada que
	// almacena los elementos de la pila. the linked list

	private int numItems; // indica la cantidad de elementos que tiene la pila.

	private int capacity; // indica la capacidad maxima de la pila

	/**
	 * Constructor de la clase PilaSobreListasEnlazadas. @pre. true. @post. Se
	 * inicializa la estructura, dejando tope en null y numItems en 0.
	 */
	public Pila(int capacity) {
		if(capacity <=0)
			throw new PreconditionException("capacidad cero o negativa");
		top = null;
		numItems = 0;
		this.capacity = capacity;
		//instrumented
		imprimir_estado();


	}

	/**
	 * inserta item al tope de la pila de elementos.
	 * 
	 * @param item es el objeto a insertar en la pila. @pre. true @post. Si la
	 *             estructura subyacente a la pila no está agotada en espacio, se
	 *             inserta item al tope de la pila. Si la inserción falla,por algun
	 *             motivo lanza una excepción
	 */
	public void push(Integer item) {
		if (!this.push_pre())
			throw new PreconditionException("violacion de precondicion Push ");
		Entry nuevoNodo = new Entry();
		nuevoNodo.setInfo(item);
		nuevoNodo.setNext(top);
		top = nuevoNodo;
		numItems++;
		//instrumented
		imprimir_estado();

	}

	/**
	 * elimina el elemento al tope de la pila. @pre. longitud()>0 @post. si la pila
	 * es no vacía, elimina el elemento del tope Si la pila está vacía
	 */
	public void pop() {
		if (!this.pop_pre())
			throw new PreconditionException("violacion de precondicion Pop ");
		top = top.getNext();
		numItems--;
		//instrumented
		imprimir_estado();

	}

	/**
	 * retorna el elemento al tope de la pila.
	 * 
	 * @return el elemento al tope de la pila. @pre.longitud()>0 @post. si la pila
	 *         es no vacía, se retorna el item del tope. Si la pila está vacía,
	 *         se lanza una excepción de tipo PilaException.
	 */
	/*public Integer top() {
		if (!this.top_pre())
			throw new PreconditionException("violacion de precondicion Top ");
		return top.getInfo();

	}*/

	/**
	 * Indica si la representación de la pila es internamente consistente.
	 * 
	 * @return true si y sólo si la representación de la pila es internamente
	 *         consistente. @pre. true @post. retornar true si y sólo si la
	 *         representación de la pila es internamente consistente. En este caso,
	 *         la estructura es internamente consistente si y sólo si numItems es
	 *         exactamente el número de nodos en la lista enlazada, desde tope y
	 *         además la estructura de datos (Lista simplemente encadenada sobre la
	 *         que se implementa) es aciclica.
	 */
	@CheckRep
	public boolean repOk() {
		Entry m = this.top;

		Set<Entry> visited = new java.util.HashSet<Entry>();
		visited.add(this.top);
		int acyclicSize = 0;
		while (m != null) {
			acyclicSize++;
			m = m.getNext();
			if (!visited.add(m))
				return false;
		}

		if (this.numItems != acyclicSize) {
			return false;
		}
		if (this.numItems > this.capacity) {
			return false;
		}

		return true;
	}

	private boolean pop_pre() {
		return numItems > 0;
	}

	private boolean push_pre() {
		return numItems < capacity;
	}
	
	
	private void imprimir_estado(){
		String res ="";
		String[] state = new String[2];
		int i =0;
		if(pop_pre()) {
			state[i] = "pop ";
			i++;
		}
		if(push_pre()) {
			state[i] = "push ";
		}

		for(String s: state) {
			if (s!= null)
				res=res + " " + s;
		}
		System.out.println(res);


	}

}
