package studycases;

import randoop.*;

public class File {

	private boolean isOpened;
	private boolean isCreated;

	public File() {
		isOpened = false;
		isCreated = true;
		//instrumented
		imprimir_estado();
	}
	
	public void open() {
		if (!this.open_pre())
			throw new PreconditionException("violacion de precondicion open ");
		isOpened = true;
		//instrumented
		imprimir_estado();
	}

	public void close() {
		if (!this.close_pre())
			throw new PreconditionException("violacion de precondicion close ");
		isOpened = false;
		//instrumented
		imprimir_estado();
	}

	public void write(byte[] text) {
		if (!this.write_pre())
			throw new PreconditionException("violacion de precondicion write "); 
		//writes a byte in the file
		//instrumented
		imprimir_estado();
	}

	public void read() { 
		if (!this.read_pre())
			throw new PreconditionException("violacion de precondicion read ");
		//reads a byte of the file
		//instrumented
		imprimir_estado();
	}

	@CheckRep
	public boolean inv() { return true;}
	
	private boolean open_pre() { return !isOpened && isCreated;}
	
	private boolean close_pre() { return isOpened; }
	
	private boolean write_pre() { return isOpened; }
	
	private boolean read_pre() { return isOpened; }
	
	private void imprimir_estado(){
		String res ="";
		String[] state = new String[4];
		int i =0;
		if(open_pre()) {
			state[i] = "open ";
			i++;
		}
		if(close_pre()) {
			state[i] = "close ";
			i++;
		}
		if(write_pre()) {
			state[i] = "write ";
			i++;
		}
		if(read_pre()) {
			state[i] = "read ";
			i++;
		}
		for(String s: state) {
			if (s!= null)
				res=res + " " + s;
		}
		System.out.println(res);
	}
}