package studycases;

import randoop.*;

/**
 * Clase Controlador: Define el controlador de un brazo robotico encargado de llenar, cargar y despachar cajas de una fabrica.
 */
public class Controlador {

	private boolean hayCaja;
	private boolean cajaCerrada;
	private boolean cajaVacia;

	public Controlador() {
		hayCaja = false;
		//instrumented
		imprimir_estado();

	}

	public void tomarCaja() {
		if (!this.tomarCaja_pre())
			throw new PreconditionException("violacion de precondicion tomarCaja ");
		hayCaja = true;
		cajaCerrada = false;
		cajaVacia = true;
		//instrumented
		imprimir_estado();

	}

	public void cargar(int peso) {
		if (!this.cargar_pre())
			throw new PreconditionException("violacion de precondicion cargar ");
		cajaVacia = false;
		//instrumented
		imprimir_estado();

	}

	public void vaciar() {
		if (!this.vaciar_pre())
			throw new PreconditionException("violacion de precondicion vaciar ");
		cajaVacia = true;
		//instrumented
		imprimir_estado();

	}

	public void cerrar() {
		if (!this.cerrar_pre())
			throw new PreconditionException("violacion de precondicion cerrar ");
		cajaCerrada = true;
		//instrumented
		imprimir_estado();

	}

	public void abrir() {
		if (!this.abrir_pre())
			throw new PreconditionException("violacion de precondicion abrir ");
		cajaCerrada = false;
		//instrumented
		imprimir_estado();

	}

	public void despachar() {
		if (!this.despachar_pre())
			throw new PreconditionException("violacion de precondicion despachar ");
		hayCaja = false;
		//instrumented
		imprimir_estado();

	}

	@CheckRep
	public boolean inv() {
		return !hayCaja || !(cajaVacia && cajaCerrada);
	}

	private boolean tomarCaja_pre() {
		return !hayCaja;
	}

	private boolean cargar_pre() {
		return hayCaja && !cajaCerrada;
	}

	private boolean vaciar_pre() {
		return hayCaja && !cajaCerrada && !cajaVacia;
	}

	private boolean cerrar_pre() {
		return hayCaja && !cajaCerrada && !cajaVacia;
	}

	private boolean abrir_pre() {
		return hayCaja && cajaCerrada;
	}

	private boolean despachar_pre() {
		return hayCaja && cajaCerrada;
	}
	
	
	private void imprimir_estado(){
		String res ="";
		String[] state = new String[6];
		int i =0;
		if(tomarCaja_pre()) {
			state[i] = "tomarCaja ";
			i++;
		}
		if(vaciar_pre()) {
			state[i] = "vaciar ";
			i++;
		}
		if(cerrar_pre()) {
			state[i] = "cerrar ";
			i++;
		}
		if(abrir_pre()) {
			state[i] = "abrir ";
			i++;
		}
		if(despachar_pre()) {
			state[i] = "despachar ";
			i++;
		}
		if(cargar_pre()) {
			state[i] = "cargar ";
		}
		for(String s: state) {
			if (s!= null)
				res=res + " " + s;
		}
		System.out.println(res);
	}

	

}
