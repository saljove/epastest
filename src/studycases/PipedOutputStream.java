package studycases;

public class PipedOutputStream {

	private boolean closedByWriter;

	private boolean closedByReader;

	private boolean connected;

	private /*Thread*/ int readSide;

	private boolean readSide_alive;

	private int PIPE_SIZE;

	private byte [] buffer;

	private int in;

	private int out;

	private void sink_receive(int b){
	  in = out + 1;
	  if (in < 0) {
	      in = 0;
	      out = 0;
	    }
	  buffer[in++] = (byte) (b & 0xFF);
	  if (in >= PIPE_SIZE){
	      in = 0;
	    }
	}

	private void sink_receivedLast (){
	  closedByWriter = true;
	}

	private boolean sink_null;

	public /*void*/ PipedOutputStream(){
	  sink_null = true;
	  imprimir_estado();
	}

	private boolean pre_connect(){
	  return sink_null;
	}

	public void connect () {
	  if(!pre_connect()) {
			throw new PreconditionException("violacion de precondicion connect ");
	  }	
	  closedByWriter = false;
	  closedByReader = false;
	  readSide = 0/*null*/;
	  readSide_alive = false;
	  PIPE_SIZE = 1024;
	  buffer = new byte[PIPE_SIZE]; //calloc (PIPE_SIZE, sizeof (byte));
	  in = -1;
	  out = 0;
	  connected = true;
	  sink_null = false;
	  imprimir_estado();
	}
	

	private boolean pre_write(){
		
		return !sink_null && connected && !closedByWriter && !closedByReader 
	    && (readSide == 0/*null*/ || readSide_alive);
	}

	public void write (int b){
	  if(!pre_write()) {
			throw new PreconditionException("violacion de precondicion write ");
	  }	
	
	  sink_receive(b);
	  imprimir_estado();
	}
	
	private boolean pre_flush(){
	  return true;
	}

	public void flush (){
	  if (!sink_null){}
	  imprimir_estado();
	}

	private boolean pre_close(){
	  return true;
	}

	public void close(){
	  if (!sink_null){
	      sink_receivedLast();
	  }
	  imprimir_estado();
	}

	private boolean invariant(){
	  return true;
	}

	private void imprimir_estado(){
		String res ="";
		String[] state = new String[4];
		int i =0;
		if(pre_write()) {
			state[i] = "write ";
			i++;
		}
		if(pre_close()) {
			state[i] = "close ";
			i++;
		}
		if(pre_flush()) {
			state[i] = "flush ";
			i++;
		}
		if(pre_connect()) {
			state[i] = "connect ";
		}

		for(String s: state) {
			if (s!= null)
				res=res + " " + s;
		}
		System.out.println(res);
	}

	
	
}
