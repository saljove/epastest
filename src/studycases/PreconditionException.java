package studycases;

public class PreconditionException extends RuntimeException {

	/**
	 * Método para trabajar excepciones por pre condiciones que se rompan.
	 */
	private static final long serialVersionUID = 1L;

	public PreconditionException(String s) {
		super(s);
	} // fin constructor

}
