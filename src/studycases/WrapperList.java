package studycases;

import randoop.CheckRep;

public class WrapperList {
	private SinglyLinkedList l;

	
	public WrapperList() {
		l = new SinglyLinkedList();
    	imprimir_estado();

	}
	
    @CheckRep
    private boolean repOk() {
    	return l == null || l.getSize() >=0;
    }
    
    private boolean add_pre() {
    	return l!=null;
    }
    
    public void add(int data){
       	if(!add_pre()) {
    		throw new PreconditionException("violacion de precondicion add");
    	}
       	try {
       		l.add(data);
       	}catch(java.lang.OutOfMemoryError e) {
       		l =null;
        }
    	imprimir_estado();

    }
    
    private boolean remove_pre() {
    	return l!=null && l.getSize() >0;
    }
    
    public void remove() {
    	if(!remove_pre()) {
    		throw new PreconditionException("violacion de precondicion remove");
    	}
    	l.remove();
    	imprimir_estado();

    }
    
    private boolean destroy_pre() {
    	return l!=null;
    }
    
    public void destroy() {
    	if(!destroy_pre()) {
    		throw new PreconditionException("violacion de precondicion destroy");
    	}
    	l=null;
    	imprimir_estado();
    }

	private void imprimir_estado(){
		String res ="";
		String[] state = new String[3];
		int i =0;
		if(add_pre()) {
			state[i] = "add ";
			i++;
		}
		if(remove_pre()) {
			state[i] = "remove ";
			i++;
		}
		if(destroy_pre()) {
			state[i] = "destroy ";
		}

		for(String s: state) {
			if (s!= null)
				res=res + " " + s;
		}
		System.out.println(res);
	}
	
}
