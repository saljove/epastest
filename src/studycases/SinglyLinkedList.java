package studycases;

public class SinglyLinkedList {

    private Node first;

    private int size = 0;
    
    
    public int getSize() {
    	return size;
    }
    
    public SinglyLinkedList() {
    	first = null;
    	size = 0;
    }
    
    public void add(int data){
    	Node newNode = new Node();
    	newNode.setData(data);
    	
	    if (first ==null) {
	    		first =newNode;
	   	}else {
	    	Node tmp = first;	
	    	Node previous = first;
	    	while(tmp!=null) {
		   		previous =tmp;
		   		tmp =tmp.getNext();
		   	}
		   	previous.setNext(newNode);
	   	}
	   	size++;
    	
    }
    
    public void remove() {
    	if(size >0) {
	    	Node newFirst = first.getNext();
	    	first = newFirst;
	    	size--;
	    }
    }
    
}