package enabledMovement;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Estructura en la que se guarda la traza de ejecución de un test
 * @author Salvador Jove, Mathias Bottero
 * @version 1.0
 */
public class EnabledMovementList implements Iterable<EnabledMovement>{
	
	/*Almacena los movimientos habilitados*/
	private Set<EnabledMovement> enabledMovementList;
	
	/**
	 * Método que inicia la construcción de la EnabledMovementList	
	 */
	public EnabledMovementList() {
		this.enabledMovementList = new HashSet<EnabledMovement>();
	}
	
	/**
	 * Método que agrega todos los elementos de un listado de movimientos	
	 * @param other parámetro que define un listado de movimientos
	 */
	public void addAll(EnabledMovementList other) {
		for(EnabledMovement s:other) {
			this.add(s);
		}
	}	

	/**
	 * Método que define la cantidad de elementos de un listado de movimientos	
	 * @return el tamaño del listado
	 */
	public int size() {
		return enabledMovementList.size();
	}

	/**
	 * Método que controla si un elemento pertenece al listado	
	 * @param sequence parámetro que define una movimiento habilitado
	 * @return si el listado contiene el movimiento
	 */
	public boolean contains(EnabledMovement enabledMovement) {
		return enabledMovementList.contains(enabledMovement);
	}
	
	/**
	 * Método que agerga un elemento al listado de movimientos	
	 * @param enabledMovement parámetro que define una movimiento habilitado
	 */
	public void add(EnabledMovement enabledMovement) {
		enabledMovementList.add(enabledMovement);
	}
	
	@Override
	public String toString() {
		return "enabledMovementList=" + enabledMovementList ;
	}
	
	@Override
	  public Iterator<EnabledMovement> iterator() {
	        return enabledMovementList.iterator();
	  }

}
