package enabledMovement;

/**
 * Clase que representa una secuencia entre dos estados
 * @author Salvador Jove, Mathias Bottero
 * @version 1.0
 */
public class EnabledMovement {	
	
	/**
	 * Representan los elementos dentro de un movimiento
	 */
	private String start;
	private String instruction;
	private String end;
	
	/**
	 * Método que inicia la construcción del EnabledMovement
     * @param start parámetro que define el primer valor de la secuencia
     * @param ins parámetro que define el segundo valor de la secuencia
     * @param end parámetro que define el tercer valor de la secuencia
     */
	public EnabledMovement(String start,String ins,String end) {
		this.start = start;
		this.instruction = ins;
		this.end = end;
	}
	
	/**
	 * Método que inicia la construcción del EnabledMovement
     */
	public EnabledMovement() {
		this.start = null;
		this.instruction = null;
		this.end = null;
	}
	
	/**
	 * Método que retorna el primer elemento de un movimiento	
	 * @return primer elemento
	 */
	public String getStart() {
		return start;
	}

	/**
	 * Método que setea el primer elemento de un movimiento	
	 * @param start parámetro que define el primer valor de un movimiento
	 */
	public void setStart(String start) {
		this.start = start;
	}

	/**
	 * Método que retorna el segundo elemento de un movimiento	
	 * @return segundo elemento
	 */
	public String getInstruction() {
		return instruction;
	}

	/**
	 * Método que setea el segundo elemento de un movimiento	
	 * @param instruction parámetro que define el segundo valor de un movimiento
	 */
	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}
	
	/**
	 * Método que retorna el tercer elemento de un movimiento	
	 * @return tercer elemento
	 */
	public String getEnd() {
		return end;
	}

	/**
	 * Método que setea el tercer elemento de un movimiento	
	 * @param end parámetro que define el tercer valor de un movimiento
	 */
	public void setEnd(String end) {
		this.end = end;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((instruction == null) ? 0 : instruction.hashCode());
		result = prime * result + ((start == null) ? 0 : start.hashCode());
		result = prime * result + ((end == null) ? 0 : end.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EnabledMovement other = (EnabledMovement) obj;
		if (instruction == null) {
			if (other.instruction != null)
				return false;
		} else if (!instruction.equals(other.instruction))
			return false;
		if (start == null) {
			if (other.start != null)
				return false;
		} else if (!start.equals(other.start))
			return false;
		if (end == null) {
			if (other.end != null)
				return false;
		} else if (!end.equals(other.end))
			return false;
		return true;
	}	

	@Override
	public String toString() {
		return "("+ start + ", " + instruction + ", " + end + ")";
	}

}
