package utils;

/**
 * Clase que representa una tupla
 * @author Salvador Jove, Mathias Bottero
 * @version 1.0
 */
public class Tuple<A,B> {
	
	private A _first;	
	private B _second;
		
	/**
	 * Método que inicia la construcción de la clase tupla
	 * @param fst parámetro que define el primer valor de la tupla
     * @param snd parámetro que define el segundo valor de la tupla
	 */
	public Tuple(A fst, B snd) {
		_first = fst;
		_second = snd;
	}
	
	/**
	 * Método que retorna el primer elemento	
	 * @return primer elemento
	 */
	public A first() { 
		return _first;
	}
	
	/**
	 * Método que retorna el segundo elemento
	 * @return segundo elemento
	 */
	public B second() { 
		return _second;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_first == null) ? 0 : _first.hashCode());
		result = prime * result + ((_second == null) ? 0 : _second.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tuple<?, ?> other = (Tuple<?, ?>) obj;
		if (_first == null) {
			if (other._first != null)
				return false;
		} else if (!_first.equals(other._first))
			return false;
		if (_second == null) {
			if (other._second != null)
				return false;
		} else if (!_second.equals(other._second))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "<" +_first + ", "+_second + ">";
	}
	
	
	
}
