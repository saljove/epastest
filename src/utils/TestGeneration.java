package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Clase que define la ejecución de randoop para un caso de estudio y ejecuta los test suite de salida.
 * @author Salvador Jove, Mathias Bottero
 * @version 1.0
 */
public class TestGeneration {
	
	private String fullClassName;
	private String separator;
	/**
	 * Método que inicia la construcción de la clase TestGeneration.
	 * @param classname parámetro que representa el nombre de la clase.
	 * @throws ClassNotFoundException cuando classname no es encontrada.
	 */
	public TestGeneration(String classname) throws ClassNotFoundException {
		this.fullClassName = classname;
	}
	
	/**
	 * Método que genera los test suit validando previamente el sistema operativo
	 * @param timelimit parámetro que representa el tiempo de ejecucion de randoop
	 * @throws ClassNotFoundException si la clase fullClassName (Clase bajo test) no es encontrada
	 */
	public void generateTest(Integer timelimit, String literalFile, String classpath) throws ClassNotFoundException {
		if (OSValidator.isMac() || OSValidator.isUnix()) {
			separator = ":";
		} else {
			separator = ";";
		}
		executeRandoop(timelimit, literalFile, classpath);
		executeTests(classpath);
	}
	
	/**
	 * Helper methods
	 * **/
	
	/**
	 * Método que ejecuta Randoop para un caso de estudio.
	 * @param timelimit parámetro que representa el tiempo de ejecución de randoop
	 * @throws ClassNotFoundException si la clase fullClassName no es encontrada.
 	 * @exception IOException en error de entrada.
	 */
	private void executeRandoop(Integer timelimit, String literalFile, String classpath) throws ClassNotFoundException {
		try {	
			String cp = "bin" +separator+ "lib/randoop-all-3.0.8.jar"+separator+ "epasTest.jar";
			if(classpath !=null) 
					cp = cp + separator+classpath;
			
			String cmd = "java -classpath "+ cp + " randoop.main.Main gentests --testclass="+fullClassName+" --timelimit="+timelimit+" "
							+ "--unchecked-exception=INVALID --no-regression-assertions=TRUE --junit-output-dir=output "
							+ "--junit-package-name="+getPackageName()+" --checked-exception=INVALID ";
			
			if (literalFile!=null) {
				File tmpDir = new File(literalFile);
				if(!tmpDir.exists()) {
					throw new Error("file " + literalFile + " not found");
				}
				cmd = cmd + " --literals-level=ALL --literals-file=" + literalFile;
			}
					
			Process randoop = Runtime.getRuntime().exec(cmd);			
			BufferedReader input = new BufferedReader(new InputStreamReader(randoop.getInputStream()));
			input.close();
			randoop.waitFor();		

			if (randoop.exitValue()!=0) 
				throw new Error("Error in test generation with randoop");
			
		} catch (Exception ioe) {
			System.out.println (ioe);
		} 
	}
	
	/**
	 * Método que ejecuta las salida de los test.	 
	 * @exception IOException en error de entrada.
	 */
	private void executeTests(String classpath) {		
		try {
			String path = getPackageName().replace(".", "/");
			
			String cp = "lib/ant-launcher.jar";
			String cmd = "java -cp " + cp + " -Dant.home=lib -DTEST_PATH="+ path+ " -DCLASS_PATH=" + classpath +" org.apache.tools.ant.launch.Launcher"; 
			runProcess(cmd);							   													
		}	
        catch (Exception e) {			
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param command 
	 * @param printLines
	 * @exception IOException en error de entrada.
	 */
      private  void runProcess(String command) throws Exception {
        Process pro = Runtime.getRuntime().exec(command);
        
        BufferedReader in = new BufferedReader(new InputStreamReader(pro.getErrorStream()));
 		in.close();
        pro.waitFor();
        if (pro.exitValue()!=0) 
			throw new Error("Error in " + command + " exitValue() " + pro.exitValue());
      }
  	
  	private  String getPackageName() throws ClassNotFoundException {
		return Class.forName(fullClassName).getPackage().getName();
  	}
}