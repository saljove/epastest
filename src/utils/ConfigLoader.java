package utils;

/**
 * Esta clase retorna el nombre de el o los constructores y
 * los metodos publicos declarados de la clase de entrada utilizando
 * la libreria reflect.
 * @author Salvador Jove, Mathias Bottero
 * @version 1.0
 */
import java.lang.reflect.*;
import java.util.ArrayList;
import main.EPAGeneration;

public class ConfigLoader {

	private static String className;
	private static Class aClass;
	
	/**
	* Método que inicia la construcción de la clase ConfigLoader.
    * @param cn. El parámetro cn define el nombre de la clase.
	*/
	public ConfigLoader(String cn) throws ClassNotFoundException {
		ClassLoader classLoader = EPAGeneration.class.getClassLoader();
		className = cn;
		aClass = classLoader.loadClass(className);
	}
	
	/**
	 * Método que retorna solo los métodos públicos de la clase.	
	 * @return un arreglo de métodos.
	 */
	public ArrayList<String> getMethods() {
		ArrayList<String> arrayMethods = new ArrayList<String>();
		Method[] Methods = aClass.getDeclaredMethods();
		for (Method method : Methods) {
			if (Modifier.isPublic(method.getModifiers())) {
				arrayMethods.add(method.getName());
			}
		}
		return arrayMethods;
	}
	
	/**
	 * Método que retorna el/los constructor/es.
	 * @return un arreglo de constructores.
	 */	
	public ArrayList<String> getConstructors() {
		ArrayList<String> arrayConstructors = new ArrayList<String>();
		Constructor[] Constructors = aClass.getConstructors();
		for (Constructor constructor : Constructors) {
			if (Modifier.isPublic(constructor.getModifiers())) {
				arrayConstructors.add(constructor.getName());
			}
		}
		return arrayConstructors;
	}

}
