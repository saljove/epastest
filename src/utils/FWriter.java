package utils;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * En esta clase se definen los métodos para la escritura de un archivo
 * @author Salvador Jove, Mathias Bottero
 * @version 1.0
 */
public class FWriter {
	
	/**
	 * Método que escribe un archivo
	 * @param path párametro que define la ruta donde se encuentra el archivo a escribir.
	 * @param code párametro que representa lo que se escribira en el archivo. 
	 * @exception IOException en error de entrada.
	 * @see IOException
	 */
	public  static void writeToFile(String path, String code) throws Exception {
		try {
	        File newTextFile = new File(path);
	        FileWriter fw = new FileWriter(newTextFile);
	        fw.write(code);
	        fw.close();
		} catch (IOException io) {
			throw io;
	    }
	}

}
