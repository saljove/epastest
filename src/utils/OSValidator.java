package utils;

/**
 * En esta clase se definen las funciones para determinar el sistema operativo que se esta utilizando
 * @author Salvador Jove, Mathias Bottero
 * @version 1.0
 */
public class OSValidator {

	private static String OS = System.getProperty("os.name").toLowerCase();
	
	/**
	 * Método que controla si el sistema operativo es windows
	 * @return true o false si el sistema operativo es windows
	 */
	public static boolean isWindows() {
		return (OS.indexOf("win") >= 0);
	}

	/**
	 * Método que controla si el sistema operativo es macintosh
	 * @return true o false si el sistema operativo es macintosh
	 */
	public static boolean isMac() {
		return (OS.indexOf("mac") >= 0);
	}

	/**
	 * Método que controla si el sistema operativo es linux
	 * @return true o false si el sistema operativo es linux
	 */
	public static boolean isUnix() {
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0 );
	}

}
