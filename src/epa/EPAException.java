package epa;

/**
 * Clase que representa una excepcion de la EPA.
 * @author Salvador Jove, Mathias Bottero
 * @version 1.0
 */
public class EPAException extends Exception {

	private static final long serialVersionUID = 1L;

	
	public EPAException() {
		super();
	}

	public EPAException(String message) {
		super(message);
	}

	
}
