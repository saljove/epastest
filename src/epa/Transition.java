package epa;

/**
 * Clase que representa una transicion
 * @author Salvador Jove, Mathias Bottero
 * @version 1.0
 */
public class Transition {	
	
	private Node node_1;
	private String instruction;
	private Node node_2;
		
	/**
	 * Método que inicia la construcción de la clase transition
	 * @param node1 parámetro que representa el nodo 1
     * @param ins parámetro que representa la instrucción
     * @param node2 parámetro que representa el nodo 2
	 */
	public Transition(Node node1,String ins,Node node2) {
		this.node_1 = node1;
		this.instruction = ins;
		this.node_2 = node2;
	}
	
	/**
	 * Método que retorna el primer Nodo	
	 * @return primer nodo
	 */
	public Node getNode_1() {
		return node_1;
	}

	/**
	 * Método que setea el primer Nodo
	 * @param node_1 parámetro que representa un nodo
	 */
	public void setNode_1(Node node_1) {
		this.node_1 = node_1;
	}

	/**
	 * Método que retorna una instrucción	
	 * @return una instruccion
	 */
	public String getInstruction() {
		return instruction;
	}

	/**
	 * Método que setea una instrucción	
	 * @param instruction parámetro que representa una instrucción
	 */
	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}

	/**
	 * Método que retorna el segundo nodo	
	 * @return segundo nodo
	 */
	public Node getNode_2() {
		return node_2;
	}

	/**
	 * Método que setea el segundo nodo	
	 * @param node_2 parámetro que representa un nodo
	 */
	public void setNode_2(Node node_2) {
		this.node_2 = node_2;
	}

	@Override
	public String toString() {
		return node_1.getName() + " --> " + instruction + " --> " + node_2.getName();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((instruction == null) ? 0 : instruction.hashCode());
		result = prime * result + ((node_1 == null) ? 0 : node_1.hashCode());
		result = prime * result + ((node_2 == null) ? 0 : node_2.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transition other = (Transition) obj;
		if (instruction == null) {
			if (other.instruction != null)
				return false;
		} else if (!instruction.equals(other.instruction))
			return false;
		if (node_1 == null) {
			if (other.node_1 != null)
				return false;
		} else if (!node_1.equals(other.node_1))
			return false;
		if (node_2 == null) {
			if (other.node_2 != null)
				return false;
		} else if (!node_2.equals(other.node_2))
			return false;
		return true;
	}

}

