package epa;

/**
 * Clase que representa un nodo.
 * @author Salvador Jove, Mathias Bottero
 * @version 1.0
 */
public class Node {
	
	private String name;
	private boolean isInitial;

	/**
	 * Método que inicia la construcción para la clase nodo
	 * @param n nombre que representa al nodo
	 */
	public Node(String n) {
		isInitial =false;
		this.name = n;
	}	
	
	/**
	 * Método que indica un nodo inicial	
	 * @param b parámetro que define un booleano
	 */
	public void setInitial(boolean b) {
		isInitial =b;
	}

	/**
	 * Método que retorna el nombre de un nodo	
	 * @return nombre del nodo
	 */
	public String getName() {
		return name;
	}

	/**
	 * Método que setea el nombre de un nodo	
	 * @param node parámetro que define el nombre de un nodo
	 */
	public void setName(String node) {
		this.name = node;
	}

	/**
	 * Método que retorna si es inicial	
	 * @return si es inicial o no
	 */
	public boolean isInitial() {
		return isInitial;
	}

	@Override
	public String toString() {
		return "<name=" + name + ", isInitial=" + isInitial + ">";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node other = (Node) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
}

