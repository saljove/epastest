package epa;

import java.util.ArrayList;
import utils.FWriter;

/**
 * Clase que representa una EPA, compuesta por estados y transiciones
 * @author Salvador Jove, Mathias Bottero
 * @version 1.0
 */
public class EPA {
	
	/*Almacena los Nodos y Transiciones de la clase de interés*/
	private ArrayList<Node> nodes; 	
	private ArrayList<Transition> transitions;

	/**
	 * Método que inicia la construcción de la clase EPA	 
	 */
	public EPA() {
		this.nodes = new ArrayList<Node>();
		this.transitions = new ArrayList<Transition>();
	}
	
	/**
	 * Método que retorna el numero de nodos	
	 * @return cantidad de nodos
	 */
	public int numberOfNodes() {
		return nodes.size();
	}
	
	/**
	 * Método que retorna el número de transiciones	
	 * @return numero de transiciones
	 */
	public int numberOfTransitions() {
		return transitions.size();
	}
	
	/**
	 * Método que retorna el nodo inicial	
	 * @return nodo inicial
	 */
	public Node initialState() {
		for(Node n: this.nodes) {
			if(n.isInitial()) {
				return n;
			}
		}
		return null;
	}

	/**
	 * Método que agrega un nodo	
	 * @param e parámetro que representa un nodo
	 */
	public void addNode(Node e) {
		if(!containsNode(e)) {
			this.nodes.add(e);
		}		
	}
	
	/**
	 * Método que agrega una transicion	
	 * @param t parámetro que representa una transicion
	 * @exception EPAException por estado no válido.
	 */
	public void addTransition(Transition t) throws EPAException {
		if(!containsTransition(t)) {
			if(!this.nodes.contains(t.getNode_1()))
				throw new EPAException("Transicion con estado no valido");
			if(!this.nodes.contains(t.getNode_2()))
				throw new EPAException("Transicion con estado no valido");
			this.transitions.add(t);
		}
	}
	
	/**
	 * Método que controla si la lista de nodos contiene un nodo	
	 * @param e parámetro que representa un nodo
	 * @return si el nodo pertenece al listado de nodos
	 */
	public boolean containsNode(Node e) {
		for(Node n: nodes) {
			if(n.equals(e)) 
				return true;
		}
		return false;
	}	
	
	/**
	 * Método que controla si la lista de transitions contiene dicha transicion	
	 * @param t parámetro que representa una transicion
	 * @return si la transicion pertenece al listado de transiciones
	 */
	public boolean containsTransition(Transition t) {
		for(Transition a: transitions) {
			if(a.equals(t)) 
				return true;
		}
		return false;
	}		
	
	/**
	 * Método que retorna el ultimo nodo	
	 * @return ultimo nodo
	 */
	public Node lastNode() {
		return nodes.get(nodes.size()-1);				
	}
	
	/**
	 * Método que imprime el listado de estados y transiciones	
	 */
	public void print() {		
		System.out.println("---- Listado de Estados -----");
		for(Node n: this.nodes) {
			System.out.println(n.toString());
		}
		System.out.println("");
		System.out.println("---- Listado de Transiciones (Estado->Transicion->Estado) ----");
		for(Transition t: this.transitions) {
			System.out.println(t.toString());
		}
	}
	
	/**
	 * Método que imprime una representación de estados y transicion en formato DOT
	 * @return representacion de nodos y transiciones
	 */
	public String toDot() {
		//Initial State is added
		String dot = "digraph {\n";
		dot += "inic[shape=point];\n";
		
		String  initial = "\""+ this.initialState().getName() +"\"";
		dot += "inic->" + initial + ";\n";
		
		//Transitions are added
		for (Transition t : this.transitions) {
			  dot += "\""+t.getNode_1().getName()+"\"" + "->" + "\""+t.getNode_2().getName()+"\"" + " [label=\"" + t.getInstruction() + "\"];\n";
		}
		return dot + "}";
	}
	
	/**
	 * Método que escribe la representación DOT en un archivo
	 * @param outputFile parámetro que representa un archivo
	 */
	public void generateDOT(String outputFile){
		String dot =toDot();
		try {
			FWriter.writeToFile(outputFile, dot);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
}
