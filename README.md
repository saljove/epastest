# Construccion de EPAs basadas en tests unitarios

A menudo, nos encontramos con implementaciones de modulos de software o APIs que carecen de especificaciones y documentacion y que no ayudan a propios (y mucho menos a extraños) al entendimiento de lo que dicho software busca resolver ni a conocer la relacion de orden entre las invocaciones de sus metodos publicos. Las EPAs ayudan a ese entendimiento y a validar el modelo mental del desarrollador, mediante una maquina de estados que contiene infomacion a cerca del comportamiento que tiene el componente en cuestion con respecto al orden valido de invocacion de sus metodos.

**_EpaTest_**
Es una herramienta que dado un caso de estudio, logra mediante la utilizacion de herramientas de testing, procesar y obtener una EPA la cual nos indica las secuencias posibles de invocacion a metodos de un modulo de software.

## Comenzando

Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas.

## Pre-requisitos 📋

Para el uso de la herramienta es necesario contar con los siguiente incisos:

Sistemas Operativos
- Linux(Ubuntu)
- MacOs

Herramientas
- Java 8+
- Grahpviz (https://graphviz.org/download/)

## Instalación 🔧

Para la instalacion de la herramienta solo debes clonar el proyecto, de dicho respositorio, en una carpeta de tu ordenador

## Ejecutando la herramienta ⚙️

Para la ejecución de la herramienta Epatest debe tener en cuenta el siguiente comando con sus respectivos parámetros:
1. Nombre de la clase
2. Tiempo de ejecucion de la herramienta randoop
3. Path al archivo de literales (opcional)
4. Classpath: path a los binarios

Los casos de prueba nombrados en la documentación son los siguientes:

- studycases.Controlador
- studycases.PilaSobreListaEnlazada
- studycases.PipedOutputStream
- studycases.SinglyLinkedList
- studycases.Signature
- studycases.File

Comando para la ejecución de Epatest
```
./run.sh <Nombre de la Clase> <Tiempo limite en segundos> <Path al archivo de literales> <Classpath: path a los binarios>
```
_Pueden ver como ejemplo literals.txt en la raiz de este repositorio como ejemplo_

(Recuerde que el `<Path a el archivo de literales>` es opcional. `<Path a el archivo de literales>` en el caso de que se requiera el uso adicional de literales. `<Classpath: path a los binarios>` en el caso de que se requiera ejecutar un caso de prueba adicional a los listados en la documentacion, deber para indicar el classpath de la clase que desee probar. A continuacion se mencionara un ejemplo de uso.)

**_Ejemplos de Uso_**
```
./run.sh studycases.Controlador 2
```
```
./run.sh studycases.PilaSobreListasEnlazadas 3 literals.txt
```
```
./run.sh miPila.utils.Pila 3 miequipo/pila/literals.txt /miequipo/pila/bin
```

Nota:
`Para el uso de la herramienta en la ejecucion de un caso de estudio diferente a los listados, debe tener en cuanta lo mencionado en la documentacion, en como especificar e instrumentar la clase.`

## Analisis del Resultado 📌

En la carpeta output encontramos el .pdf que nos genera la herramienta, indicando la EPA resultante.

## Autores ✒️

* **Mathias Bottero** - *Implementacion y Documentacion*
* **Salvador Jove** - *Implementacion y Documentación*

## Expresiones de Gratitud 🎁

Queremos agradecer a nuestar coordinadora de Tesis **Valeria Vengolea**, por la ayuda y dedicacion que nos brindo durante toda la preparacion.
