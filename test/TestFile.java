
import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;


import epa.EPA;
import epa.EPAException;
import main.EPAGeneration;

public class TestFile {

	@Test
	public void testFile()  {
		//1 second should be enough
		String className = "studycases.File";
		EPA epa = EPAGeneration.start(className,1,null,null);
		epa.generateDOT("output/"+ className+".dot");
		assertTrue(epa.numberOfNodes()==3 && epa.numberOfTransitions()==4);
	}
	
}
