
import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import epa.EPA;
import epa.EPAException;
import main.EPAGeneration;

public class TestPilaPrueba {


	@Test
	public void testPila()  {
		//here we need extra literals for randoop
		String className = "miPila.utils.Pila";
		EPA epa = EPAGeneration.start(className,3,"literals.txt",null);
		epa.generateDOT("output/"+ className+".dot");
		assertTrue(epa.numberOfNodes()==4 && epa.numberOfTransitions()==9);
	}
		
}
