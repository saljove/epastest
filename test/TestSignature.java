//package studycases;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;
import epa.EPA;
import epa.EPAException;
import main.EPAGeneration;

public class TestSignature {
	
	@Test
	public void testMySignature() {

		String className = "studycases.MySignature";
		EPA epa = EPAGeneration.start(className,2,null,null);
		epa.generateDOT("output/"+ className+".dot");
		assertTrue(epa.numberOfNodes()==4 && epa.numberOfTransitions()==11);
	}
	

}
