//package studycases;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;
import epa.EPA;
import epa.EPAException;
import main.EPAGeneration;

public class TestPipedOutputStream {

	
	@Test
	public void testPipedOutputStream()  {
		String className = "studycases.PipedOutputStream";
		EPA epa = EPAGeneration.start(className,1,null,null);
		epa.generateDOT("output/"+ className+".dot");
		assertTrue(epa.numberOfNodes()==4 && epa.numberOfTransitions()==9);
	}
	
	
}
