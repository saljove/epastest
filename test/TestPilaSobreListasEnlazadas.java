//package studycases;

import static org.junit.Assert.*;


import org.junit.Test;
import epa.EPA;
import main.EPAGeneration;

public class TestPilaSobreListasEnlazadas {
	
	@Test
	public void testPilasSobreListasEnlazada()  {
		//here we need extra literals for randoop
		String className = "studycases.PilaSobreListasEnlazadas";
		EPA epa = EPAGeneration.start(className,3,"literals.txt", null);
		epa.generateDOT("output/"+ className+".dot");
		assertTrue(epa.numberOfNodes()==4 && epa.numberOfTransitions()==9);
	}
	
}
