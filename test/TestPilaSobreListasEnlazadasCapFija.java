//package studycases;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;
import epa.EPA;
import epa.EPAException;
import main.EPAGeneration;

public class TestPilaSobreListasEnlazadasCapFija {
	
	@Test
	public void testPilasSobreListasEnlazadaCapFija()  {
		String className = "studycases.PilaSobreListasEnlazadasCapFija";
		EPA epa = EPAGeneration.start(className,5,null,null);
		epa.generateDOT("output/"+ className+".dot");
		assertTrue(epa.numberOfNodes()==4 && epa.numberOfTransitions()==7);
	}
		
}
